This project is on "guided run-and-tumble active particles". It is written in OCTAVE (>5.0), and has been developed and utilized during the following research:

[Guided run-and-tumble active particles: wall accumulation and preferential deposition](https://pubs.rsc.org/en/content/articlelanding/2021/sm/d1sm00775k/unauth), Soft Matter, 17(39), pp. 8858-8866, 2021.

A brief description of the program and project is provided below.

![](description.jpg) 

To go to the original article: [click here](https://pubs.rsc.org/en/content/articlelanding/2021/sm/d1sm00775k/unauth).



If using the code, please include the following in your copy.

    bio3d Copyright (C) 2021  Chamkor Singh

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
