function [ex_,ey_,ez_]=rotate_vec(ex,ey,ez,theta)

  [nx ,ny ,nz ] = getRandAxisPerpToUnitVec(ex,ey,ez);
  [ex_,ey_,ez_] = rotUnitVecAroundPerpAxis(ex,ey,ez,nx,ny,nz,theta);

endfunction

function [nx,ny,nz]=getRandAxisPerpToUnitVec(ex,ey,ez)

  N = length(ex);
  i = 1:N;
  
while true  
  
  rx(i)=2*(rand(1,N)-0.5);
  ry(i)=2*(rand(1,N)-0.5);
  rz(i)=2*(rand(1,N)-0.5);
  r=sqrt(rx.^2+ry.^2+rz.^2);
  rx=rx./r;
  ry=ry./r;
  rz=rz./r;

  r_dot_e = rx.*ex+ry.*ey+rz.*ez;
  i       = find(abs(r_dot_e)>0.9);
  N       = length(i);
  
  if isempty(i)
    break;  
  endif

endwhile

  nx=rx-(rx.*ex+ry.*ey+rz.*ez).*ex;
  ny=ry-(rx.*ex+ry.*ey+rz.*ez).*ey;
  nz=rz-(rx.*ex+ry.*ey+rz.*ez).*ez;
  n=sqrt(nx.^2+ny.^2+nz.^2);
  nx=nx./n;
  ny=ny./n;
  nz=nz./n;

endfunction

function [ex_,ey_,ez_] = rotUnitVecAroundPerpAxis(ex,ey,ez,nx,ny,nz,theta)
  
  ex_ = ex.*cosd(theta) + (ny.*ez-nz.*ey).*sind(theta);
  ey_ = ey.*cosd(theta) + (nz.*ex-nx.*ez).*sind(theta);
  ez_ = ez.*cosd(theta) + (nx.*ey-ny.*ex).*sind(theta);
    
endfunction