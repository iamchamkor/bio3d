function [ex_,ey_,ez_] = rotate_vec_guiding_torque(ex,ey,ez,c,gamma_r,dt)
  
      %normalize
      einitial = sqrt(ex.^2+ey.^2+ez.^2);
      ex=ex./einitial;
      ey=ey./einitial;
      ez=ez./einitial;
  
      Np  = length(ex);           
      phi = acosd(-ez);
      
      if phi==180 || phi==0 
        phi=phi+1e-8; %to avoid division by zero;
      end
      
      %rotation axis
      rotaxis_x = +ey./sind(phi);
      rotaxis_y = -ex./sind(phi);
      rotaxis_z = zeros(1,Np); %no rotation about z
      %raxis = sqrt(rotaxis_x.^2+rotaxis_y.^2+rotaxis_z.^2)
      
      dtheta = -(c/gamma_r) *sind(phi) *dt; %from langevin equation
      
      [ex_,ey_,ez_] = rotate_vec_given_axis(ex,ey,ez,rotaxis_x,rotaxis_y,rotaxis_z,dtheta);
      %efinal = sqrt(ex_.^2+ey_.^2+ez_.^2) 
end      
      
function [ex_,ey_,ez_] = rotate_vec_given_axis(ex,ey,ez,nx,ny,nz,theta)
  
  ex_ = ex.*cosd(theta) + (ny.*ez-nz.*ey).*sind(theta);
  ey_ = ey.*cosd(theta) + (nz.*ex-nx.*ez).*sind(theta);
  ez_ = ez.*cosd(theta) + (nx.*ey-ny.*ex).*sind(theta);
    
endfunction