function [issticking,colonyid] = stick2surface(issticking,z,Lz,diameter,P_single,colonyid)

for i=find ( z <= -(0.5*Lz-0.5*diameter) )

    if issticking(i)~=1
      ps=rand;
      if ps<P_single
        issticking(i)=1;  
        colonyid(i)  =i;
      endif
    endif

endfor

endfunction