function [fx,fy,fz] = force(x,y,z,Lx,Ly,Lz,Nx,Ny,Nz,diameter,force_coeff)
    
    [head,next] = genlinklist(x,y,z,Lx,Ly,Lz,Nx,Ny,Nz);  
    [fx,fy,fz]  = getforce(x,y,z,Lx,Ly,Lz,Nx,Ny,Nz,head,next,diameter,force_coeff);

end

function [fx,fy,fz] = getforce(x,y,z,LX,LY,LZ,NX,NY,NZ,head,next,diameter,force_coeff)
    
    NP = length(x);
    
    fx=zeros(1,NP);
    fy=zeros(1,NP);
    fz=zeros(1,NP);

    % Scan inner cells
    for (ix_cell=0:NX-1)
    for (iy_cell=0:NY-1)
    for (iz_cell=0:NZ-1)

    % Calculate a scalar cell index
    i_cell = ix_cell + NX*iy_cell + NX*NY*iz_cell + 1; % +1 to match matlab indexing

        % Scan the neighbor cells (including itself) of cell c
        for (ix_cell_neighbour=ix_cell-1 : ix_cell+1)
        for (iy_cell_neighbour=iy_cell-1 : iy_cell+1)
        for (iz_cell_neighbour=iz_cell-1 : iz_cell+1)

        rshift_x = 0.0; 
        rshift_y = 0.0; 
        rshift_z = 0.0;
        
        % Periodic boundary condition by shifting coordinates
        if(ix_cell_neighbour < 0)
            rshift_x = -LX;        
        elseif(ix_cell_neighbour >= NX)
            rshift_x = +LX;        
        else
            rshift_x = 0.0;
        end

        %comment out rshift_y if bcs are not periodic in y        
        if(iy_cell_neighbour < 0)
            rshift_y = -LY;        
        elseif(iy_cell_neighbour >= NY)
            rshift_y = +LY;        
        else
            rshift_y = 0.0;
        end

##        %comment out rshift_z if bcs are not periodic in z         
##        if(iz_cell_neighbour < 0)
##            rshift_z = -LZ;        
##        elseif(iz_cell_neighbour >= NZ)
##            rshift_z = +LZ;        
##        else
##            rshift_z = 0.0;
##        end

        % Calculate the scalar cell index of the neighbor cell
        i_cell_neighbour = rem((iz_cell_neighbour+NZ),NZ)*(NX*NY)...
                              +rem((iy_cell_neighbour+NY),NY)*(NX)...
                                +rem((ix_cell_neighbour+NX),NX)...
                                  +1; % +1 to match matlab indexing
        
        % Scan particle i in cell c
        i = head(i_cell);
        while (i ~= -1)
          
            % Scan particle j in cell c1
            j = head(i_cell_neighbour);
            while (j ~= -1) 

                if (i < j) % to avoid double counting of pair (i, j)

                    rx_ji = x(i) - (x(j) + rshift_x); % Image-corrected relative pair position, r_ji = r_i - r_j in the direction from particle j to i
                    ry_ji = y(i) - (y(j) + rshift_y); % Image-corrected relative pair position, r_ji = r_i - r_j in the direction from particle j to i
                    rz_ji = z(i) - (z(j) + rshift_z); % Image-corrected relative pair position, r_ji = r_i - r_j in the direction from particle j to i

                    r_ji = sqrt(rx_ji*rx_ji + ry_ji*ry_ji + rz_ji*rz_ji);

                    if (r_ji < diameter)
                        
                          %Compute forces on pair (i, j)                        
                          overlap = diameter - r_ji;
                            
                          %force
                          fi    = force_coeff*overlap;
                          
                          nx    = rx_ji/r_ji;
                          ny    = ry_ji/r_ji;
                          nz    = rz_ji/r_ji;
                          
                          fx(i) = fx(i) + fi*nx;
                          fy(i) = fy(i) + fi*ny;
                          fz(i) = fz(i) + fi*nz; 
 
                          fx(j) = fx(j) - fi*nx;
                          fy(j) = fy(j) - fi*ny;
                          fz(j) = fz(j) - fi*nz;  
                                              
                    end
                end

                j = next(j);
            end

            i = next(i);
        end

        end
        end
        end

    end
    end
    end
    
end


function [head,next] = genlinklist(x,y,z,Lx,Ly,Lz,Nx,Ny,Nz)

    Np=length(x);
    
    head   = zeros(1,Nx*Ny*Nz);
    next   = zeros(1,Np);
    
    head(:) = -1;

    %Vector cell index to which this particle belongs    
    ibox=floor( (x./Lx + 0.5).*Nx);
    jbox=floor( (y./Ly + 0.5).*Ny);
    kbox=floor( (z./Lz + 0.5).*Nz);
        
    %Translate the vector cell index, mc, to a scalar cell index
    boxindex=(ibox+jbox.*Nx+kbox.*Nx.*Ny) + 1; % +1 due to matlab indexing, starts from 1
    
    for i=1:Np   
        %Link to the previous occupant (or EMPTY if you're the 1st), but name it "next" which is 
        %convenient while unrolling the list
        next(i) = head(boxindex(i));

        %The last one goes to the header, the list starts from the head while unrolling the list
        head(boxindex(i)) = i;
    end
    
end
