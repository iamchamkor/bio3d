function [issticking,colonyid] = stick2colony(issticking,x,y,z,diameter,P_single,P_colony,lambda,size,colonyid)

for i=find ( issticking == 1 )
for j=find ( issticking ~= 1 )

                    rx_ji = x(i) - x(j); 
                    ry_ji = y(i) - y(j); 
                    rz_ji = z(i) - z(j);

                    r_ji = sqrt(rx_ji*rx_ji + ry_ji*ry_ji + rz_ji*rz_ji);  

                    if (r_ji <= diameter)                      
                      
                      s=size(colonyid(i));
                      P = P_colony/(1+ ((P_colony - P_single)/P_single)*exp(-lambda*s) );
                      %P=0.08;
                      ps=rand;
                      if ps<P
                        issticking(j)=1;  
                        colonyid(j)=colonyid(i);                        
                      endif
                      
                    endif
                    
endfor
endfor

endfunction