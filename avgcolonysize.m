function [s,Ns,c,Nc] = avgcolonysize(colonyid,Np)
%function [s,Ns,c,Nc,c_std,Nc_std,c_pdf,Nc_pdf,c_avg] = avgcolonysize(colonyid,Np)
  
for i=1:Np
  s(i)  = length( find(colonyid==i) );
end

for i=1:Np
  Ns(i) = length( find(s==i) );
end

c  = 1:Np;
##c  = c(find(Ns~=0));
Nc = Ns;#(find(Ns~=0));

##M2 = sum(c.*c.*Nc);
##M1 = sum(c.*Nc);
##
##if (M1==0)
##  c_avg = 0;
##else
##  c_avg = M2/M1;
##endif
##
##%pdf
##Nc_std = std(Nc);
##c_std  = std(c);
##
##if c_std==0
##  c_pdf  = 0;
##  Nc_pdf = 0;
##else
##  %c_std  = std(c);
##  c_pdf  = c./c_std;
##  No     = sum(Nc.*c)./c_std;
##  Nc_pdf = Nc./No;
##endif

endfunction