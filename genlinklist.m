function [head,next] = genlinklist(x,y,z,Lx,Ly,Lz,Nx,Ny,Nz)

    Np=length(x);
    
    head   = zeros(1,Nx*Ny*Nz);
    next   = zeros(1,Np);
    
    head(:) = -1;

    %Vector cell index to which this particle belongs    
    ibox=floor( (x./Lx + 0.5).*Nx);
    jbox=floor( (y./Ly + 0.5).*Ny);
    kbox=floor( (z./Lz + 0.5).*Nz);
        
    %Translate the vector cell index, mc, to a scalar cell index
    boxindex=(ibox+jbox.*Nx+kbox.*Nx.*Ny) + 1; % +1 due to matlab indexing, starts from 1
    
    for i=1:Np   
        %Link to the previous occupant (or EMPTY if you're the 1st), but name it "next" which is 
        %convenient while unrolling the list
        next(i) = head(boxindex(i));

        %The last one goes to the header, the list starts from the head while unrolling the list
        head(boxindex(i)) = i;
    end
    
end
