clear all;
close all;
clc;

format long;
pkg load statistics;


%create output folder
base_folder="output/output-sticking-pareto-0-r-0";
counter=1;
while counter>0
if ~exist(sprintf("%s/run-%d",base_folder,counter), 'dir')
       mkdir(sprintf("%s/run-%d",base_folder,counter));
       output_folder=sprintf("%s/run-%d",base_folder,counter);
       break;
else
counter=counter+1;
endif
endwhile
output_folder


%parameters
tmax            =1000; %sec
Np              =500;  %number of particles
Lx              =30;   %micrometer
Ly              =30;   %micrometer
Lz              =30;   %micrometer
Nxboxes         =16;
Nyboxes         =16;
Nzboxes         =16;    
dt              =0.025;%sec
diameter        =1.0;  %micrometer
D               =0.05; %kB*T/gamma_t, translational diffusion coefficient
gamma_t         =1.0;  %translational friction
force_coeff     =gamma_t/(2*dt); %used in excluded volume
Dr              =0.05; %kB*T/gamma_r, rotational diffusion coefficient

tau_run_mean    =0.86; %sec, mean run duration
tau_tumble_mean =0.14; %sec, mean tumble duration
dtheta_mean     =60;   %degree, mean change in angle during tumble
dtheta_std      =20;   %degree, std of change in angle during tumble
v_o             =10;   %bacteria swimming speed, micrometer/sec

pareto_scale = tau_run_mean;  %heavy tailed generalized Pareto distribution, scale parameter
pareto_shape = 0;             %heavy tailed generalized Pareto distribution, shape parameter, =0 for exponential distribution
torque_c     = 0;             %guiding torque, in output folder naming, it is notated as "r"
gamma_r      = 1;             %rotational friction coefficient


theta     =zeros(1,Np); 
counter   =zeros(1,Np); counter(:)=0;
sigma     =zeros(1,Np); sigma(:)  =1;
fi_x      =zeros(1,Np);
fi_y      =zeros(1,Np);
delta     =zeros(1,Np);
issticking=zeros(1,Np);
colonyid  =zeros(1,Np);
x         =zeros(1,Np);
y         =zeros(1,Np);
z         =zeros(1,Np);
ex        =zeros(1,Np);
ey        =zeros(1,Np);
ez        =zeros(1,Np);

%initial conditions
x    =Lx*(rand(1,Np)-0.5);
y    =Ly*(rand(1,Np)-0.5); 
z    =Lz*(rand(1,Np)-0.5);  
ex   =2*(rand(1,Np)-0.5);
ey   =2*(rand(1,Np)-0.5);
ez   =2*(rand(1,Np)-0.5);
e =sqrt(ex.^2+ey.^2+ez.^2);
ex=ex./e;
ey=ey./e;
ez=ez./e;

    
t=0; n=0;
while (t<tmax)    
    
      for i = find(counter<=0)
        dsigma   = sigma(i);
        sigma(i) = 1-sigma(i);
        dsigma   = sigma(i)-dsigma;
        
        if dsigma==1                      %beginning of run
        #tau_run   = exprnd(tau_run_mean); %model for exponential distribution with mean tau_run [Berg & Brown 1972]
        tau_run   = gprnd(pareto_shape,pareto_scale,0.0); %pareto model for heavy tail distribution with arguments (shape, scale, location=0) [Korobkova et al. 2004]
        #csvwrite('output/run_time.csv', [tau_run],'-append'); % to check run time distribution, turn off in production runs
        counter(i)= round(tau_run/dt);    %no. of steps in this run
        randsign  = sign((rand-0.5));
        dtheta    = randsign*abs(dtheta_mean+dtheta_std*randn()) *(1-issticking(i)); %from a normal distribution bw [0,180]
        #csvwrite('output/angle_change.csv', [dtheta],'-append'); % to check tumble angle distribution, turn off in production runs
        [ex(i),ey(i),ez(i)]=rotate_vec(ex(i),ey(i),ez(i),dtheta);      
        endif

        if dsigma==-1                       %beginning of tumble
        tau_tumble=exprnd(tau_tumble_mean); %from exp. distribution with mean tau_tumble
        #csvwrite('output/tumble_time.csv', [tau_tumble],'-append'); % to check tumble time distribution, turn off in production runs
        counter(i)=round(tau_tumble/dt);    %no. of steps in this tumble  
        endif
      endfor
      
      %rotational diffusion
      dtheta = sqrt(2*Dr*dt).*randn(1,Np)*180/pi;
      [ex,ey,ez]=rotate_vec(ex,ey,ez,dtheta.*(1-issticking));
      
      #rotation due to guiding field acting along -z direction / guiding torque perp. to z 
      [ex,ey,ez]= rotate_vec_guiding_torque(ex,ey,ez,torque_c,gamma_r,dt);
            
      %translation
      x=x+ (sqrt(2*D*dt).*randn(1,Np) + sigma.*v_o.*ex*dt) .*(1-issticking); %sigma=0 during tumbles
      y=y+ (sqrt(2*D*dt).*randn(1,Np) + sigma.*v_o.*ey*dt) .*(1-issticking); %sigma=0 during tumbles
      z=z+ (sqrt(2*D*dt).*randn(1,Np) + sigma.*v_o.*ez*dt) .*(1-issticking); %sigma=0 during tumbles 
  
      %counter
      counter=counter-1;
            
      %pbcs---
      x=Lx*(x/Lx-round(x/Lx)); %x(find(x==0.5*Lx))=-0.5*Lx;
      y=Ly*(y/Ly-round(y/Ly)); %y(find(y==0.5*Ly))=-0.5*Ly;
      #z=Lz*(z/Lz-round(z/Lz)); %z(find(z==0.5*Lz))=-0.5*Lz;
      
      %reflective bcs in z
      i=find(abs(round(z/Lz)) == 1);  
      z(i)  = z(i)-2*(abs(z(i))-0.5*Lz).*round(z(i)/Lz);
      ez(i) = -ez(i);       
      %check z bottom again
      k=find(z<-0.5*Lz);
      z(k)=z(k)+2*(abs(z(k))-0.5*Lz);
      ez(k) = -ez(k);
      
      
      %overlap normal force/excluded volume
      [fi_x,fi_y,fi_z] = force(x,y,z,Lx,Ly,Lz,...
                                   Nxboxes,Nyboxes,Nzboxes,...
                                   diameter,force_coeff);
    
    
      %twitching motion
      x=x + dt*fi_x/gamma_t;
      y=y + dt*fi_y/gamma_t; 
      z=z + dt*fi_z/gamma_t;  

      
      %cluster statistics
      %turn off below lines if sticking is to be turned off
      [size,Ns,c,Nc] = avgcolonysize(colonyid,Np);
      P_single = 0.004; %preferential deposition model parameters
      P_colony = 0.4;   %preferential deposition model parameters
      lambda   = 0.4;   %preferential deposition model parameters
      [issticking,colonyid] = stick2surface(issticking,z,Lz,diameter,P_single,colonyid);
      [issticking,colonyid] = stick2colony(issticking,x,y,z,diameter,P_single,P_colony,lambda,size,colonyid);
    

      %save data every n iterations
      if(rem(n,10)==0)              
        t
        csvwrite(sprintf("%s/scatter.csv",output_folder),             [t Lx Ly Lz x y z ex ey ez issticking],'-append');         
        csvwrite(sprintf("%s/p_deposited.csv",output_folder),         [t length(find(issticking==1))],'-append');              
        csvwrite(sprintf("%s/size_dist.csv",output_folder),           [t c Nc],'-append'); 
        csvwrite(sprintf("%s/state_run_or_tumble.csv",output_folder), [t sigma],'-append');        
      endif
      
      
      %update time    
      t=t+dt;
      n=n+1;
  
endwhile